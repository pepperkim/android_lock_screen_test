package com.pepper.lockscreentest

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPermission()
    }

    private fun checkPermission(){
        if( !Settings.canDrawOverlays(this)){
            val uri = Uri.fromParts("package", packageName, null)
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, uri)
            startActivityResult.launch(intent)
        } else {
            val serviceIntent = Intent( applicationContext, LockScreenService::class.java)
            startForegroundService(serviceIntent)
        }
    }

    private var startActivityResult = registerForActivityResult(StartActivityForResult()) { result ->
        if(result.resultCode == 0) {
            if (!Settings.canDrawOverlays(this)) {
                Toast.makeText(this, "Do it", Toast.LENGTH_LONG).show()
            } else {
                val serviceIntent = Intent(applicationContext, LockScreenService::class.java)
                startForegroundService(serviceIntent)
            }
        }
    }
}